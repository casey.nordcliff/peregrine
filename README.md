# Peregrine

<p align="center">
  <img src="assets/peregrine.png" width="300" />
</p>

<p align="center">
  <sub>(Image credit to Josh Brill of https://dribbble.com/joshbrill)</sub>
</p>


Peregrine is a dependency-free program written in Rust that very quickly
counts the number of primes in a given range.

## Compiling / Building

Simply run `compile.sh` to call out to Cargo to build the project.

## Usage

```bash
./compile.sh
./target/release/peregrine <upper limit>
```

```
# Example using Peregrine to count primes up to 1 billion
time ./target/release/peregrine 1000000000
50847534
./target/release/peregrine 1000000000  4.97s user 0.10s system 99% cpu 5.080 total
```

## Contributing

If you have an idea that you would like to see, or a bug that you found, please
feel free to make an issue. If you have improvement / performance ideas, please
feel free to make a PR.
