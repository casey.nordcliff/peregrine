# Build Peregrine using the Cargo with --release optimizations
cargo build --release

# Strip debugging symbols from the compiled binary to reduce file size
strip target/release/peregrine

# Displays information about the compiled binary
ls -al target/release/peregrine
