use std::env;

fn main() {
    // Parse and validate command line arguments
    let args: Vec<String> = env::args().collect();
    let limit: usize = args[1].parse().unwrap();

    // Only allocate memory for the number 2 and all odd numbers
    let size;
    match limit % 2 {
        0 => size = limit / 2,
        _ => size = (limit + 1) / 2
    }
    let mut is_prime: Vec<u8> = vec![1; size];

    // Begin with an array of true values. The first element is true,
    // and therefore prime, but every multiple of that element is not.
    // Move on to the next true value until you run out.
    let mut i = 3;
    while i < limit {
        if is_prime[i / 2] == 1 {
            let mut j = i * i;
            while j < limit {
                is_prime[j / 2] = 0;
                j = j + (2 * i);
            }
        }
        i = i + 2;
    }

    // is_prime loses all its false value, giving us an array of true values
    is_prime.retain(|&x| x == 1);
    println!("{}", is_prime.len());
}
